import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './layouts/auth/auth.component';
import { PreloginComponent } from './layouts/prelogin/prelogin.component';
import { AuthRoutingModule } from './modules/auth/auth-routing.,module';
import { AuthModule } from './modules/auth/auth.module';
import { ViewerComponent } from './layouts/viewer/viewer.component';
import { ViewerRoutingModule } from './modules/viewer/viewer-routing.module';
import { ViewerModule } from './modules/viewer/viewer.module';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    PreloginComponent,
    ViewerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthRoutingModule,
    ViewerRoutingModule,
    ViewerModule,
    AuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
