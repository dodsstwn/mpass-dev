import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { TYPE } from 'src/app/constant/type.constant'
import { environment } from 'src/environments/environment'
import { ENDPOINT } from 'src/app/constant/endpoint.constant'

type HttpOption = {
  headers: HttpHeaders
}

type PostParam = {
  username?: string | null
  password?: string | null
}

@Injectable()
export class AuthService {

  basePath = environment.baseUrl
  
  httpOptions: HttpOption
  constructor(private readonly httpClient: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        contentType: TYPE.APP_JSON
      })
    }
  }

  doCallSignInApi(data: PostParam) {
    return this.httpClient.post(
      `${this.basePath}${ENDPOINT.SIGN_IN}`,
      data,
      {
        headers: new HttpHeaders({
          contentType: TYPE.APP_JSON,
        })
      }
    )
  }

}
