import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { TYPE } from 'src/app/constant/type.constant'
import { environment } from 'src/environments/environment'
import { ENDPOINT } from 'src/app/constant/endpoint.constant'

type HttpOption = {
  headers: HttpHeaders
}

type DetailParam = {
  id?: string | null
}

@Injectable()
export class PromoService {

  basePath = environment.baseUrl
  
  httpOptions: HttpOption
  constructor(private readonly httpClient: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        contentType: TYPE.APP_JSON
      })
    }
  }

  doCallPopularPromos() {
    return this.httpClient.get(
      `${this.basePath}${ENDPOINT.POPULAR_PROMO}`,
      {
        headers: new HttpHeaders({
          contentType: TYPE.APP_JSON,
          authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjc1OTE0MTUwLCJleHAiOjE2Nzg1MDYxNTB9.TcIgL5CDZYg9o8CUsSjUbbUdsYSaLutOWni88ZBs9S8'
        })
      }
    )
  }

  doCallDetailPromo(data: DetailParam) {
    return this.httpClient.get(
      `${this.basePath}${ENDPOINT.DETAIL_PROMO}/${data}`, this.httpOptions
    )
  }

}
