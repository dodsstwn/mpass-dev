import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AuthComponent } from 'src/app/layouts/auth/auth.component'
import { LoginComponent } from './login/login.component'
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component'


const AuthRoutes: Routes = [
  {
    path: '',
    redirectTo: 'sign-in',
    pathMatch: 'full'
  },
  {
    path: '',
    title: 'Auth',
    component: AuthComponent,
    children: [
      {
        path: 'sign-in',
        // canActivate: [AuthLoginGuard],
        title: 'MPASS - Sign In',
        component: LoginComponent
      },
      {
        path: 'forgot-password',
        title: 'MPASS - Forgot Password',
        // canActivate: [AuthLoginGuard],
        component: ForgotPasswordComponent
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(AuthRoutes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}
