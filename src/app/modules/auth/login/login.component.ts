import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  })
  
  constructor(
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly authService: AuthService
  ) {

  }
  
  ngOnInit(): void {
    localStorage.clear()
  }

  doRedirectForgotPassword() {
    this.router.navigateByUrl('/forgot-password')
  }

  doLogin() {
    const postParam = {
      username: this.loginForm.value?.username,
      password: this.loginForm.value?.password
    }
    this.authService.doCallSignInApi(postParam).subscribe({
      next: (resp: any) => {
        const userData = resp?.data
        localStorage.setItem('token', userData.token)
        localStorage.setItem('fullname', userData.fullname)
        localStorage.setItem('email', userData.email)
        this.router.navigateByUrl('/user/home')
      },
      error: (err: any) => {
        console.error(err)
      }
    })
  }

}
