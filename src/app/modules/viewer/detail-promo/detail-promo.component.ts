import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PromoService } from 'src/app/core/services/promos.service';

@Component({
  selector: 'app-detail-promo',
  templateUrl: './detail-promo.component.html',
  styleUrls: ['./detail-promo.component.scss']
})
export class DetailPromoComponent implements OnInit {

  detailId: any = sessionStorage.getItem('idSelect')
  detailData: any

  constructor(
    private readonly router: Router,
    private readonly promoService: PromoService,
    private readonly _location: Location
  ) {
    
  }

  ngOnInit(): void {
    this.doGetDetailPromo()
  }

  doGetDetailPromo() {
    this.promoService.doCallDetailPromo(this.detailId).subscribe({
      next: (resp: any) => {
        this.detailData = resp?.data
      },
      error: (err: any) => {
        console.error(err)
      }
    })
  }
  
  goBack() {
    this._location.back()
  }

  goToOrderLocation() {
    sessionStorage.setItem('priceSelected', this.detailData?.price)
    sessionStorage.setItem('locStoreSelected', this.detailData?.storeLocation)
    this.router.navigateByUrl('/user/order-location')
  }

}
