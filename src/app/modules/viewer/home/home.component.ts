import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PromoService } from 'src/app/core/services/promos.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  fullName = localStorage.getItem('fullname')
  listCategory = [
    {
      name: 'Beverages',
      icon: 'food'
    },
    {
      name: 'Gadget',
      icon: 'phone'
    },
    {
      name: 'Transportation',
      icon: 'car'
    }
  ]

  listPopularPromo: any = []

  constructor(
    private readonly promoService: PromoService,
    private readonly router: Router
  ) {

  }

  ngOnInit(): void {
    this.getPopularPromos()
  }

  getPopularPromos() {
    this.promoService.doCallPopularPromos().subscribe({
      next: (resp: any) => {
        this.listPopularPromo = resp?.data
      },
      error: (err: any) => {
        console.error(err)
      }
    })
  }

  goToDetailPage(data: any) {
    sessionStorage.setItem('idSelect', data)
    this.router.navigateByUrl(`user/detail-promo`)
  }
}
