import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Loader } from '@googlemaps/js-api-loader';


@Component({
  selector: 'app-order-location',
  templateUrl: './order-location.component.html',
  styleUrls: ['./order-location.component.scss']
})
export class OrderLocationComponent implements OnInit {
  orderForm = this.fb.group({
    storeLocation: [sessionStorage.getItem('locStoreSelected'), Validators.required],
    userAddress: ['', Validators.required]
  })

  constructor(
    private readonly _location: Location,
    private readonly fb: FormBuilder,
    private readonly router: Router
  ) {

  }

  loader = new Loader({
    apiKey: "AIzaSyDCyQXuEP-gaDcUgTdaipVLbx8zPMK6kkQ",
    version: "weekly",
  });
  
  ngOnInit(): void {
    this.doInitMap()
  }

  goBack() {
    this._location.back()
  }


  doInitMap() {
    this.loader.load().then(async () => {
      const { Map } = await google.maps.importLibrary("maps") as google.maps.MapsLibrary;
      const map = new Map(document.getElementById("gmap") as HTMLElement, {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 8,
      });
    });
  }

  doOrder() {
    sessionStorage.setItem('userAddress', `${this.orderForm.value.userAddress}`)
    this.router.navigateByUrl('user/result')
  }

}
