import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ViewerComponent } from 'src/app/layouts/viewer/viewer.component'
import { HomeComponent } from './home/home.component'
import { DetailPromoComponent } from './detail-promo/detail-promo.component'
import { OrderLocationComponent } from './order-location/order-location.component'
import { ResultPageComponent } from './result-page/result-page.component'


const ViewerRoutes: Routes = [
  {
    path: 'user',
    component: ViewerComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'detail-promo',
        component: DetailPromoComponent
      },
      {
        path: 'order-location',
        component: OrderLocationComponent
      },
      {
        path: 'result',
        component: ResultPageComponent
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(ViewerRoutes)],
  exports: [RouterModule]
})
export class ViewerRoutingModule {}
