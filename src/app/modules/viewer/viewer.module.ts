import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { PromoService } from 'src/app/core/services/promos.service';
import { DetailPromoComponent } from './detail-promo/detail-promo.component';
import { OrderLocationComponent } from './order-location/order-location.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ResultPageComponent } from './result-page/result-page.component';



@NgModule({
  declarations: [
    HomeComponent,
    DetailPromoComponent,
    OrderLocationComponent,
    ResultPageComponent
  ],
  imports: [
    CommonModule,
    GoogleMapsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [PromoService]
})
export class ViewerModule { }
